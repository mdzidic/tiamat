---
minimum_pre_commit_version: 1.15.2
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v2.5.0
    hooks:
      - id: check-merge-conflict  # Check for files that contain merge conflict strings.
        language_version: python3
      - id: trailing-whitespace   # Trims trailing whitespace.
        args: [--markdown-linebreak-ext=md]
        language_version: python3
      - id: mixed-line-ending     # Replaces or checks mixed line ending.
        args: [--fix=lf]
        language_version: python3
      - id: end-of-file-fixer     # Makes sure files end in a newline and only a newline.
        exclude: tests/fake_.*\.key
        language_version: python3
      - id: check-ast             # Simply check whether files parse as valid python.
        language_version: python3
      - id: check-yaml
      - id: check-json

  # ----- Formatting ---------------------------------------------------------------------------->
  - repo: https://github.com/saltstack/pre-commit-remove-import-headers
    rev: 1.0.0
    hooks:
      - id: remove-import-headers

  - repo: https://github.com/asottile/pyupgrade
    rev: v2.7.2
    hooks:
      - id: pyupgrade
        name: Rewrite Code to be Py3.6+
        args: [--py36-plus]

  - repo: https://github.com/asottile/reorder_python_imports
    rev: v2.3.5
    hooks:
      - id: reorder-python-imports
        args: [--py36-plus]

  - repo: https://github.com/psf/black
    rev: 20.8b1
    hooks:
      - id: black
        language_version: python3
        files: ^(tiamat/|tests/).*\.pyi?$

  - repo: https://github.com/asottile/blacken-docs
    rev: v1.8.0
    hooks:
      - id: blacken-docs
        args: [--skip-errors]
        files: ^docs/.*\.rst
        additional_dependencies: [black==20.8b1]

  # <---- Formatting -----------------------------------------------------------------------------

  # ----- Static Code Analysis ------------------------------------------------------------------>
  - repo: https://gitlab.com/pycqa/flake8
    rev: 3.8.1
    hooks:
      - id: flake8
        language_version: python3
        additional_dependencies: [flake8-typing-imports==1.9.0]

  - repo: https://github.com/pre-commit/mirrors-mypy
    rev: v0.782  # NOTE: keep this in sync with setup.py.
    hooks:
      - id: mypy
        alias: mypy-code
        name: Run mypy against project code
        files: ^tiamat/.*\.py$
        args: [tiamat]
        pass_filenames: False
        additional_dependencies: [pop]

      - id: mypy
        alias: mypy-tests
        name: Run mypy against project tests
        files: ^tests/.*\.py$
        args: [tests]
        pass_filenames: False
        additional_dependencies: [pytest, dict-toolbox]
#  - repo: https://github.com/pre-commit/mirrors-pylint
#    rev: v2.4.4
#    hooks:
#      - id: pylint
#        name: PyLint
#        args: [--output-format=parseable, --rcfile=.pylintrc]
#        additional_dependencies:
#         - pyenchant
  # <---- Static Code Analysis -------------------------------------------------------------------

  # ----- Tests Static Requirements ------------------------------------------------------------->
  - repo: https://github.com/saltstack/pip-tools-compile-impersonate
    rev: dff9089c1003af49665064067ff63c75b9b69dcd
    hooks:
      - id: pip-tools-compile
        alias: compile-3.6-test-requirements
        name: Py3.6 Test Requirements
        files: ^requirements/(tests\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.6
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.7-test-requirements
        name: Py3.7 Test Requirements
        files: ^requirements/(tests\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.8-test-requirements
        name: Py3.8 Test Requirements
        files: ^requirements/(tests\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/tests.in

      - id: pip-tools-compile
        alias: compile-3.9-test-requirements
        name: Py3.9 Test Requirements
        files: ^requirements/(tests\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.9
          - --platform=linux
          - requirements/tests.in
  # <---- Tests Static Requirements --------------------------------------------------------------

  # ----- Docs Static Requirements -------------------------------------------------------------->
      - id: pip-tools-compile
        alias: compile-3.6-docs-requirements
        name: Py3.6 Docs Requirements
        files: ^requirements/(docs\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.6
          - --platform=linux
          - requirements/docs.in

      - id: pip-tools-compile
        alias: compile-3.7-docs-requirements
        name: Py3.7 Docs Requirements
        files: ^requirements/(docs\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.7
          - --platform=linux
          - requirements/docs.in

      - id: pip-tools-compile
        alias: compile-3.8-docs-requirements
        name: Py3.8 Docs Requirements
        files: ^requirements/(docs\.in|base\.txt)$
        pass_filenames: false
        args:
          - -v
          - --py-version=3.8
          - --platform=linux
          - requirements/docs.in

# Commenting out docs static requirements on 3.9 since it requires lxml and there isn't a wheel
# for 3.9, thus, it requires additional system development librarier. We'll worry about that once
# we start buliding docs under Py3.9
#      - id: pip-tools-compile
#        alias: compile-3.9-docs-requirements
#        name: Py3.9 Docs Requirements
#        files: ^requirements/(docs\.in|base\.txt)$
#        pass_filenames: false
#        args:
#          - -v
#          - --py-version=3.9
#          - --platform=linux
#          - requirements/docs.in
  # <---- Docs Static Requirements ---------------------------------------------------------------
