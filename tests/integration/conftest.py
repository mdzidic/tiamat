import os
import pathlib
import shutil
import sys
from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="tiamat")
    yield hub


CWD = pathlib.Path(__file__).resolve().parent


@pytest.fixture
def data_dir() -> pathlib.Path:
    return CWD / "test_data"


@pytest.fixture
def pb_conf(data_dir) -> str:
    return str(data_dir / "pb" / "pb.yml")


@pytest.fixture
def pb_bin(data_dir):
    cwd = os.getcwd()
    build_dir = data_dir / "pb" / "build"
    dist_dir = data_dir / "pb" / "dist"
    PDIR = str(data_dir / "pb")

    os.chdir(PDIR)

    with mock.patch("sys.path", sys.path + [PDIR, str(CWD.parent)]):
        yield str(dist_dir / "pb")
    os.chdir(cwd)
    shutil.rmtree(dist_dir, ignore_errors=True)
    shutil.rmtree(build_dir, ignore_errors=True)


@pytest.fixture
def sc_bin(data_dir):
    cwd = os.getcwd()
    PDIR = str(data_dir / "sc")
    build_dir = data_dir / "sc" / "build"
    dist_dir = data_dir / "sc" / "dist"
    os.chdir(PDIR)

    with mock.patch("sys.path", sys.path + [str(CWD.parent), PDIR]):
        yield str(dist_dir / "special.bin")
    os.chdir(cwd)
    shutil.rmtree(dist_dir, ignore_errors=True)
    shutil.rmtree(build_dir, ignore_errors=True)
