import pytest


def test_build(hub, mock_hub, bname):
    mock_hub.tiamat.pkg.dmg.build = hub.tiamat.pkg.dmg.build

    with pytest.raises(NotImplementedError):
        mock_hub.tiamat.pkg.dmg.build(bname)
