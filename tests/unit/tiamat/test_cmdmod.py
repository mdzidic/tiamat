import logging
import os
import subprocess
from unittest import mock

import pytest
from dict_tools import data


def test_run(hub, mock_hub):
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(dry_run=False))

    cmd = ["command", "args"]
    timeout = 999
    proc = mock.Mock()
    proc.return_value = subprocess.CompletedProcess(
        cmd, stdout=b"stdout", stderr=b"stderr", returncode=0
    )

    with mock.patch("subprocess.run", proc) as mock_run:
        ret = mock_hub.tiamat.cmd.run(cmd, timeout=timeout)

        mock_run.assert_called_once_with(
            cmd,
            shell=False,
            timeout=timeout,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            env=None,
            cwd=os.getcwd(),
        )

        assert "retcode" in ret
        assert "stdout" in ret
        assert "stderr" in ret

        assert isinstance(ret, data.NamespaceDict)


@pytest.mark.parametrize("cwd", ("/tmp/blah", "/usr/share/foo", None))
def test_run_cwd(hub, mock_hub, cwd):
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(dry_run=False))

    cmd = ["command", "args"]
    timeout = 999
    proc = mock.Mock()
    proc.return_value = subprocess.CompletedProcess(
        cmd, stdout=b"stdout", stderr=b"stderr", returncode=0
    )

    if cwd is None:
        expected_cwd = os.getcwd()
    else:
        expected_cwd = cwd
    with mock.patch("subprocess.run", proc) as mock_run:
        mock_hub.tiamat.cmd.run(cmd, timeout=timeout, cwd=cwd)

        mock_run.assert_called_once_with(
            cmd,
            shell=False,
            timeout=timeout,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            env=None,
            cwd=expected_cwd,
        )


def test_run_logs(hub, mock_hub, caplog):
    mock_hub.log.info = hub.log.info
    mock_hub.log.debug = hub.log.debug
    mock_hub.log.error = hub.log.error
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(dry_run=False))

    caplog.clear()
    with caplog.at_level(logging.DEBUG):

        cmd = ["command", "args"]
        cwd = "/tmp"
        dry_run = False
        timeout = 999
        proc = mock.Mock()
        proc.return_value = subprocess.CompletedProcess(
            cmd, stdout=b"stdout", stderr=b"stderr", returncode=0
        )

        with mock.patch("subprocess.run", proc) as mock_run:
            ret = mock_hub.tiamat.cmd.run(cmd, timeout=timeout, cwd=cwd)

            mock_run.assert_called_once_with(
                cmd,
                shell=False,
                timeout=timeout,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True,
                env=None,
                cwd=cwd,
            )

    assert caplog.records[0].levelname == "INFO"
    assert caplog.records[0].message.startswith(f"Running {cmd} in {cwd}")
    # Since the command did not fail, the expected log level is DEBUG
    assert caplog.records[1].levelname == "DEBUG"
    expected_log_message = (
        f"Command {cmd} running in {cwd} completed.\n"
        f" Dry-Run: {dry_run}\n"
        f" Exitcode: {ret.retcode}\n"
    )
    assert caplog.records[1].message.startswith(expected_log_message)

    caplog.clear()
    with caplog.at_level(logging.DEBUG):

        cmd = ["command", "args"]
        cwd = "/tmp"
        dry_run = False
        timeout = 999
        proc = mock.Mock()
        proc.return_value = subprocess.CompletedProcess(
            cmd, stdout=b"stdout", stderr=b"stderr", returncode=1
        )

        with mock.patch("subprocess.run", proc) as mock_run:
            ret = mock_hub.tiamat.cmd.run(cmd, timeout=timeout, cwd=cwd)

            mock_run.assert_called_once_with(
                cmd,
                shell=False,
                timeout=timeout,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True,
                env=None,
                cwd=cwd,
            )

    assert caplog.records[0].levelname == "INFO"
    assert caplog.records[0].message.startswith(f"Running {cmd} in {cwd}")
    # Since the command failed, the expected log level is ERROR
    assert caplog.records[1].levelname == "ERROR"
    expected_log_message = (
        f"Command {cmd} running in {cwd} completed.\n"
        f" Dry-Run: {dry_run}\n"
        f" Exitcode: {ret.retcode}\n"
    )
    assert caplog.records[1].message.startswith(expected_log_message)


def test_run_default_timeout(hub, mock_hub):
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(dry_run=False))

    cmd = ["command", "args"]
    proc = mock.Mock()
    proc.return_value = subprocess.CompletedProcess(
        cmd, stdout=b"stdout", stderr=b"stderr", returncode=0
    )

    with mock.patch("subprocess.run", proc) as mock_run:
        ret = mock_hub.tiamat.cmd.run(cmd)

        mock_run.assert_called_once_with(
            cmd,
            shell=False,
            timeout=300,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            env=None,
            cwd=os.getcwd(),
        )

        assert "retcode" in ret
        assert "stdout" in ret
        assert "stderr" in ret

        assert isinstance(ret, data.NamespaceDict)


def test_run_opts_timeout(hub, mock_hub):
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(dry_run=False, timeout=400))

    cmd = ["command", "args"]
    timeout = mock_hub.OPT.tiamat.timeout
    proc = mock.Mock()
    proc.return_value = subprocess.CompletedProcess(
        cmd, stdout=b"stdout", stderr=b"stderr", returncode=0
    )

    with mock.patch("subprocess.run", proc) as mock_run:
        ret = mock_hub.tiamat.cmd.run(cmd, timeout=timeout)

        mock_run.assert_called_once_with(
            cmd,
            shell=False,
            timeout=400,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            env=None,
            cwd=os.getcwd(),
        )

        assert "retcode" in ret
        assert "stdout" in ret
        assert "stderr" in ret

        assert isinstance(ret, data.NamespaceDict)
