FROM python:3-slim
SHELL ["/bin/bash", "-i", "-c"]
# Use musl-cross to build on other architectures: https://github.com/GregorR/musl-cross

# TODO Try this: https://github.com/JonathonReinhart/staticx/issues/143#issuecomment-675204848

RUN apt update && apt install -y busybox musl musl-dev musl-tools liblzma-dev scons patchelf python3-pip git

ENV BOOTLOADER_CC=/usr/bin/musl-gcc
ENV USE_STATIC_X=true

RUN echo 'python3 -m pip' > /usr/bin/pip && chmod +x /usr/bin/pip
RUN pip install staticx



ADD ./ /tmp/tiamat-src
RUN python3 -m pip install /tmp/tiamat-src

COPY docker/entrypoint.sh /entrypoint.sh

RUN unset LD_LIBRARY_PATH
ENTRYPOINT ["/entrypoint.sh"]
# bash: cannot set terminal process group (-1): Inappropriate ioctl for device
# bash: no job control in this shell
# [9] Error loading Python lib '/tmp/_MEIfaFKKe/libpython3.8.so.1.0': dlopen: /lib/x86_64-linux-gnu/libpthread.so.0:
# symbol __twalk_r version GLIBC_PRIVATE not defined in file libc.so.6 with link time reference
