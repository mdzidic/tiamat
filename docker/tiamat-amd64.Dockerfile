FROM cdrx/pyinstaller-linux:latest
SHELL ["/bin/bash", "-i", "-c"]

ADD . /tmp/tiamat-src
RUN /root/.pyenv/shims/python3 -m pip install /tmp/tiamat-src

COPY docker/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
